#  greedo-layout-for-ohos

#### 项目介绍
- 项目名称：greedo-layout-for-ohos
- 所属系列：openharmony的第三方组件适配移植
- 功能：根据图片比例展示图片流，固定高度展示图片流
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.5.1

#### 效果演示

![效果演示](./printscreen/screenshot.gif)

#### 安装教程

1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:greedo-layout-for-ohos:1.5.2')
    ......  
 }
```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.在布局文件中加入ListContainer控件,代码实例如下:

```
    <ListContainer
           ohos:id="$+id:listContainer"
           ohos:height="match_parent"
           ohos:width="match_parent"
           ohos:layout_alignment="horizontal_center"
           ohos:long_click_enabled="false"/>

```
2.设置适配器数据，获取 GreedoLayoutSizeCalculator对象传入最大高度，间距.详情见DEMO

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.5.2
- 0.0.1-SNAPSHOT

#### 版权和许可信息
GreedoLayout is released under the MIT license. See LICENSE for details.