/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.fivehundredpx.greedo_layout_sample;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @时间：
 * @描述：
 **/
public class GreedoLayoutSizeCalculatorTest implements GreedoLayoutSizeCalculator.SizeCalculatorDelegate {
    /**
     * 单元测试计算宽度
     */
    @Test
    public void calculateWidth() {
        GreedoLayoutSizeCalculator greedoLayoutSizeCalculator = new GreedoLayoutSizeCalculator(this);
        int itemHeight = 800;
        double aspectRatio = 1;
        int width = greedoLayoutSizeCalculator.calculateWidth(itemHeight, aspectRatio);
        assertNotNull(width);
        assertEquals(String.valueOf(itemHeight), String.valueOf(width));
    }

    /**
     * 单元测试计算高度
     */
    @Test
    public void calculateHeight() {
        GreedoLayoutSizeCalculator greedoLayoutSizeCalculator = new GreedoLayoutSizeCalculator(this);
        int itemWidth = 800;
        double aspectRatio = 1;
        int height = greedoLayoutSizeCalculator.calculateHeight(itemWidth, aspectRatio);
        assertNotNull(height);
        assertEquals(String.valueOf(itemWidth), String.valueOf(height));
    }

    @Override
    public double aspectRatioForIndex(int index) {
        return 0;
    }
}