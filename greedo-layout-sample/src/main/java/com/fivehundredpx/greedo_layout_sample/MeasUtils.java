package com.fivehundredpx.greedo_layout_sample;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * 工具类
 *
 * @author hw
 * @since 2021/05/28
 */
public class MeasUtils {
    /**
     * vp转px
     *
     * @param vp      数值
     * @param context 上下文
     * @return px值
     */
    public static int vpToPx(double vp, Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        float dpi = display.getAttributes().densityPixels;
        return (int) (vp * dpi + 0.5F * (vp >= 0 ? 1 : -1));
    }

    /**
     * 屏幕宽
     *
     * @param context 上下文
     * @return 宽度
     */
    public static int getDisplayWidthInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        return display.getAttributes().width;
    }

}
