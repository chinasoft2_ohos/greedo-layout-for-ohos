package com.fivehundredpx.greedo_layout_sample;


/**
 * 常量
 *
 * @author hw
 * @since 2021/05/28
 */
public class Constants {
    /**
     * image数值
     */
    public final static int[] IMAGES = {
            ResourceTable.Media_photo_1,
            ResourceTable.Media_photo_2,
            ResourceTable.Media_photo_3,
            ResourceTable.Media_photo_4,
            ResourceTable.Media_photo_5,
            ResourceTable.Media_photo_6,
            ResourceTable.Media_photo_7,
            ResourceTable.Media_photo_8,
            ResourceTable.Media_photo_9,
            ResourceTable.Media_photo_10,
            ResourceTable.Media_photo_11,
            ResourceTable.Media_photo_12,
            ResourceTable.Media_photo_13,
            ResourceTable.Media_photo_14,
            ResourceTable.Media_photo_15,
            ResourceTable.Media_photo_16,
            ResourceTable.Media_photo_17
    };
}
