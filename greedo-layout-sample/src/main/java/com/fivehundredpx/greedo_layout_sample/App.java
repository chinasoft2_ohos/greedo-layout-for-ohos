package com.fivehundredpx.greedo_layout_sample;

import ohos.aafwk.ability.AbilityPackage;

/**
 * 应用入口
 *
 * @author hw
 * @since 2021/05/28
 */
public class App extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
