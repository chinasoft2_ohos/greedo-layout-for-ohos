package com.fivehundredpx.greedo_layout_sample;

import com.fivehundredpx.greedo_layout_sample.slice.SampleAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

/**
 * 主要ability
 *
 * @author hw
 * @since 2021/05/28
 */
public class SampleAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.TRANSPARENT.getValue());
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS); // 沉浸式状态栏
        super.setMainRoute(SampleAbilitySlice.class.getName());
    }
}
