package com.fivehundredpx.greedo_layout_sample.view;

import com.fivehundredpx.greedo_layout_sample.ResourceTable;
import com.fivehundredpx.greedo_layout_sample.ultimateprovider.Model;
import com.fivehundredpx.greedo_layout_sample.viewholder.ItemViewHolder;
import com.fivehundredpx.greedolayout.Size;

import java.util.List;

/**
 * item的model类
 *
 * @author hw
 * @since 2021/05/28
 */
public class ItemModel implements Model {
    private List<Size> sizes;
    private List<Integer> itemResIds;

    public ItemModel(List<Size> sizes, List<Integer> itemResIds) {
        this.sizes = sizes;
        this.itemResIds = itemResIds;
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public List<Integer> getItemResIds() {
        return itemResIds;
    }

    @Override
    public int getResource(int position) {
        return ResourceTable.Layout_item_layout;
    }

    @Override
    public Class getHolderClass(int position) {
        return ItemViewHolder.class;
    }
}
