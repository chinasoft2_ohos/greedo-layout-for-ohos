package com.fivehundredpx.greedo_layout_sample.viewholder;

import com.fivehundredpx.greedo_layout_sample.ResourceTable;
import com.fivehundredpx.greedo_layout_sample.ultimateprovider.EventTransmissionListener;
import com.fivehundredpx.greedo_layout_sample.ultimateprovider.UltimateProvider;
import com.fivehundredpx.greedo_layout_sample.ultimateprovider.ViewHolder;
import com.fivehundredpx.greedo_layout_sample.view.ItemModel;
import com.fivehundredpx.greedolayout.Size;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;

import java.util.List;

/**
 * item的复用
 *
 * @author hw
 * @since 2021/05/28
 */
public class ItemViewHolder extends ViewHolder<ItemModel> {
    private DirectionalLayout layout;

    public ItemViewHolder(EventTransmissionListener eventTransmissionListener, Component component,
            UltimateProvider provider, ComponentContainer componentContainer) {
        super(eventTransmissionListener, component, provider, componentContainer);
        layout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_layout);
    }

    @Override
    public void onDataBound() {
        ItemModel itemModel = getModel();
        List<Integer> res = itemModel.getItemResIds();
        List<Size> sizes = itemModel.getSizes();
        int size = sizes.size();
        int spacing = getProvider().getSpacing();
        for (int i = 0; i < size; i++) {
            Image image = new Image(getContext());
            image.setWidth(sizes.get(i).getWidth());
            image.setHeight(sizes.get(i).getHeight());
            image.setPaddingLeft(spacing);
            if (i == size - 1) {
                image.setPaddingRight(spacing);
            }
            image.setPaddingTop(spacing);
            image.setScaleMode(Image.ScaleMode.CLIP_CENTER);
            image.setPixelMap(res.get(i));
            layout.addComponent(image);
        }
    }
}
