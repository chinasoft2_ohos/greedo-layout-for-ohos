package com.fivehundredpx.greedo_layout_sample.ultimateprovider;

/**
 * 接口定义
 *
 * @author hw
 * @since 2021/05/28
 */
public interface Notify {
    void notifyDataChanged();

    void notifyDataInvalidated();

    void notifyDataSetItemChanged(int position);

    void notifyDataSetItemInserted(int position);

    void notifyDataSetItemRemoved(int position);

    void notifyDataSetItemRangeChanged(int startPos, int countItems);

    void notifyDataSetItemRangeInserted(int startPos, int countItems);

    void notifyDataSetItemRangeRemoved(int startPos, int countItems);
}
