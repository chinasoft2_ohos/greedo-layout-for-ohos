package com.fivehundredpx.greedo_layout_sample.ultimateprovider;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.util.List;

/**
 * ViewHolder
 *
 * @param <M> 对应的模型
 * @author hw
 * @since 2021/05/28
 */
public abstract class ViewHolder<M extends Model> implements Notify {
    private M model;
    private EventTransmissionListener eventTransmissionListener;
    private Component component;
    private UltimateProvider provider;
    private int position;
    private ComponentContainer componentContainer;

    /**
     * 构造方法
     *
     * @param eventTransmissionListener 事件传输对象
     * @param component                 component
     * @param provider                  provider
     * @param componentContainer        componentContainer
     */
    public ViewHolder(EventTransmissionListener eventTransmissionListener, Component component,
            UltimateProvider provider, ComponentContainer componentContainer) {
        this.componentContainer = componentContainer;
        this.eventTransmissionListener = eventTransmissionListener;
        this.component = component;
        this.provider = provider;
    }

    /**
     * 当数据已经绑定到ViewMolder。应该在这里给UI控件设置数据
     */
    public abstract void onDataBound();

    /**
     * 返回当前模型数据
     *
     * @return 数据模型
     */
    public M getModel() {
        return model;
    }

    /**
     * 设置当前数据模型
     *
     * @param model 模型类
     */
    public void setModel(M model) {
        this.model = model;
    }

    /**
     * 获取事件传输对象
     *
     * @return 对象
     */
    public EventTransmissionListener getEventTransmissionListener() {
        return eventTransmissionListener;
    }

    /**
     * 设置事件传输对象
     *
     * @param eventTransmissionListener 对象
     */
    public void setEventTransmissionListener(EventTransmissionListener eventTransmissionListener) {
        this.eventTransmissionListener = eventTransmissionListener;
    }

    /**
     * 当前数据provider
     *
     * @return 对象
     */
    public UltimateProvider getProvider() {
        return provider;
    }

    /**
     * 设置数据provider
     *
     * @param provider 对象
     */
    public void setProvider(UltimateProvider provider) {
        this.provider = provider;
    }

    /**
     * 获取当前position
     *
     * @return 位置position
     */
    public int getPosition() {
        return position;
    }

    /**
     * 设置当前position
     *
     * @param position 对象
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * 获取Component
     *
     * @return 对象
     */
    public Component getComponent() {
        return component;
    }

    /**
     * 设置组件
     *
     * @param component 对象
     */
    public void setComponent(Component component) {
        this.component = component;
    }

    /**
     * 上下文获取
     *
     * @return 上下文
     */
    public Context getContext() {
        return component.getContext();
    }

    /**
     * 获取自定义数据
     *
     * @return 数据
     */
    public  <M> M getCustomData() {
        return (M)getProvider().getCustomData();
    }

    /**
     * 获取所有需要显示的数组
     *
     * @return 模型列表
     */
    public List getList() {
        return getProvider().getModels();
    }

    /**
     * 根据id获取控件
     *
     * @param id 控件id
     * @return 模型列表
     */
    public Component findComponentById(int id) {
        return getComponent().findComponentById(id);
    }

    /**
     * 获取componentContainer
     *
     * @return 容器组件
     */
    public ComponentContainer getComponentContainer() {
        return componentContainer;
    }

    /**
     * 设置componentContainer
     *
     * @param componentContainer 对象
     */
    public void setComponentContainer(ComponentContainer componentContainer) {
        this.componentContainer = componentContainer;
    }

    /**
     * 更新当前ItemView
     */
    public void notifyCurrentDataSetItemChanged() {
        notifyDataSetItemChanged(getPosition());
    }

    @Override
    public void notifyDataChanged() {
        getProvider().notifyDataChanged();
    }

    @Override
    public void notifyDataInvalidated() {
        getProvider().notifyDataInvalidated();
    }

    @Override
    public void notifyDataSetItemChanged(int position) {
        getProvider().notifyDataSetItemChanged(position);
    }

    @Override
    public void notifyDataSetItemInserted(int position) {
        getProvider().notifyDataSetItemInserted(position);
    }

    @Override
    public void notifyDataSetItemRemoved(int position) {
        getProvider().notifyDataSetItemRemoved(position);
    }

    @Override
    public void notifyDataSetItemRangeChanged(int startPos, int countItems) {
        getProvider().notifyDataSetItemRangeChanged(startPos, countItems);
    }

    @Override
    public void notifyDataSetItemRangeInserted(int startPos, int countItems) {
        getProvider().notifyDataSetItemRangeInserted(startPos, countItems);
    }

    @Override
    public void notifyDataSetItemRangeRemoved(int startPos, int countItems) {
        getProvider().notifyDataSetItemRangeRemoved(startPos, countItems);
    }
}
