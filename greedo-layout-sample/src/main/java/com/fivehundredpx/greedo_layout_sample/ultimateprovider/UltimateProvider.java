package com.fivehundredpx.greedo_layout_sample.ultimateprovider;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * 数据适配器
 *
 * @param <M> 模型
 * @author hw
 * @since 2021/05/28
 */
public class UltimateProvider<M extends Model> extends BaseItemProvider {
    private List<M> models;
    private Context context;
    private M customData;
    private EventTransmissionListener eventTransmissionListener;
    private int spacing;

    public int getSpacing() {
        return spacing;
    }

    public void setSpacing(int spacing) {
        this.spacing = spacing;
    }

    /**
     * 获取自定义数据
     *
     * @return 自定义数据
     */
    public <M> M getCustomData() {
        return (M)customData;
    }

    /**
     * 设置自定义数据
     *
     * @param customData 自定义数据
     */
    public void setCustomData(M customData) {
        this.customData = customData;
    }

    /**
     * 获取所有需要显示的数组
     *
     * @return 模型列表
     */
    public List<M> getModels() {
        return models;
    }

    /**
     * 模型数据
     *
     * @param models 模型列表
     */
    public void setModels(List<M> models) {
        this.models = models;
    }

    /**
     * 获取Context
     *
     * @return context
     */
    public Context getContext() {
        return context;
    }

    /**
     * 设置 context
     *
     * @param context context
     */
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * 获取事件传输对象
     *
     * @return 事件传输对象
     */
    public EventTransmissionListener getEventTransmissionListener() {
        return eventTransmissionListener;
    }

    /**
     * 设置事件传输对象
     *
     * @param eventTransmissionListener 事件传输对象
     */
    public void setEventTransmissionListener(EventTransmissionListener eventTransmissionListener) {
        this.eventTransmissionListener = eventTransmissionListener;
    }

    /**
     * 构造方法
     *
     * @param models  模型数据
     * @param context context
     */
    public UltimateProvider(List<M> models, Context context) {
        this.models = models;
        this.context = context;
    }

    @Override
    public int getCount() {
        return models == null ? 0 : models.size();
    }

    @Override
    public Object getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onItemMoved(int from, int to) {
        super.onItemMoved(from, to);
    }

    /**
     * 创建ViewHolder
     *
     * @param position           当前位置
     * @param componentContainer 父容器
     * @return 返回创建的ViewHolder
     */
    private ViewHolder createViewHolder(int position, ComponentContainer componentContainer) {
        M model = models.get(position);
        ViewHolder viewHolder = null;
        Component component = LayoutScatter.getInstance(context).parse(model.getResource(position), null, false);
        try {
            Constructor constructor = model.getHolderClass(position).getDeclaredConstructor(
                    EventTransmissionListener.class,
                    Component.class, UltimateProvider.class, ComponentContainer.class);
            viewHolder = (ViewHolder) constructor.newInstance(getEventTransmissionListener(), component,
                    this, componentContainer);
            component.setTag(viewHolder);
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException
                | InvocationTargetException e) {
            e.printStackTrace();
        }
        return viewHolder;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder;
        M model = models.get(position);
        if (component == null) {
            viewHolder = createViewHolder(position, componentContainer);
        } else {
            viewHolder = (ViewHolder) component.getTag();
            if (!viewHolder.getClass().equals(model.getHolderClass(position)) ||
                    viewHolder.getClass().getName().contains("ItemViewHolder")) {
                viewHolder = createViewHolder(position, componentContainer);
            }
        }
        viewHolder.setPosition(position);
        viewHolder.setModel(model);
        viewHolder.onDataBound();
        return viewHolder.getComponent();
    }
}
