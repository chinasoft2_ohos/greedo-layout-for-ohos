package com.fivehundredpx.greedolayout;

/**
 * Created by Julian Villella on 16-02-23.
 */
public class Size {
    int mWidth;
    int mHeight;

    /**
     * 构造方法
     *
     * @param width  宽度
     * @param height 高度
     */
    public Size(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    /**
     * 获取宽度
     *
     * @return 宽度
     */
    public int getWidth() {
        return mWidth;
    }

    /**
     * get方法
     *
     * @return 高度
     */
    public int getHeight() {
        return mHeight;
    }

    /**
     * set方法
     *
     * @param height 高度
     */
    public void setHeight(int height) {
        this.mHeight = height;
    }
}
